# Ansible Role: Terraboard

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-terraboard/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-terraboard/-/commits/main)

This role installs [Terraboard](https://terraboard.io/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    terraboard_version: latest # tag v2.0.0 if you want a specific version
    terraboard_arch: amd64 # amd64, 386, arm or arm64
    terraboard_platform: linux # linux or freebsd
    setup_dir: /tmp
    terraboard_bin_path: /usr/local/bin/terraboard
    terraboard_repo_path: https://github.com/camptocamp/terraboard/releases/download

This role can install the latest or a specific version. See [available terraboard releases](https://github.com/camptocamp/terraboard/releases/) and change this variable accordingly.

    terraboard_version: latest # tag v2.0.0 if you want a specific version

The path of the terraboard repository.

    terraboard_repo_path: https://github.com/terraboard/terraboard/releases/download

The location where the terraboard binary will be installed.

    terraboard_bin_path: /usr/local/bin/terraboard

Terraboard supports amd64, 386, arm and arm64 CPU architectures, just change for the main architecture of your CPU.

    terraboard_arch: amd64 # amd64, 386, arm arm64

Terraboard needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install terraboard. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: terraboard

## License

MIT / BSD
